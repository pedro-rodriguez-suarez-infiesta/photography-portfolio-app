import { TestBed } from '@angular/core/testing';

import { GalleryThirdService } from './gallery-third.service';

describe('GalleryThirdService', () => {
  let service: GalleryThirdService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GalleryThirdService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
