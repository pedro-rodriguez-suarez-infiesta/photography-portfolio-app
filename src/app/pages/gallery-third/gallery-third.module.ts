import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GalleryThirdRoutingModule } from './gallery-third-routing.module';
import { GalleryThirdComponent } from './gallery-third-components/gallery-third.component';


@NgModule({
  declarations: [GalleryThirdComponent],
  imports: [
    CommonModule,
    GalleryThirdRoutingModule
  ]
})
export class GalleryThirdModule { }
