import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GallerySecondComponent } from './gallery-second.component';

describe('GallerySecondComponent', () => {
  let component: GallerySecondComponent;
  let fixture: ComponentFixture<GallerySecondComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GallerySecondComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GallerySecondComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
