import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GalleryFourthRoutingModule } from './gallery-fourth-routing.module';
import { GalleryFourthComponent } from './gallery-fourth-components/gallery-fourth.component';


@NgModule({
  declarations: [GalleryFourthComponent],
  imports: [
    CommonModule,
    GalleryFourthRoutingModule
  ]
})
export class GalleryFourthModule { }
