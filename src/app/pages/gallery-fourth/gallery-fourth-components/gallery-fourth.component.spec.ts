import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GalleryFourthComponent } from './gallery-fourth.component';

describe('GalleryFourthComponent', () => {
  let component: GalleryFourthComponent;
  let fixture: ComponentFixture<GalleryFourthComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GalleryFourthComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GalleryFourthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
