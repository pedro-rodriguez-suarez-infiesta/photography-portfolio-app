import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GalleryThirdComponent } from './gallery-third-components/gallery-third.component';

const routes: Routes = [
  {
    path: 'gallerythird',
    component: GalleryThirdComponent,
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GalleryThirdRoutingModule { }
