import { TestBed } from '@angular/core/testing';

import { GallerySecondService } from './gallery-second.service';

describe('GalleryFirstService', () => {
  let service: GallerySecondService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GallerySecondService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
