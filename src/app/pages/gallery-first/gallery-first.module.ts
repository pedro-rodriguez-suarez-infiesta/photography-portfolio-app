import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GalleryFirstRoutingModule } from './gallery-first-routing.module';
import { GalleryFirstComponent } from './gallery-first-components/gallery-first.component';


@NgModule({
  declarations: [GalleryFirstComponent],
  imports: [
    CommonModule,
    GalleryFirstRoutingModule
  ]
})
export class GalleryFirstModule { }
