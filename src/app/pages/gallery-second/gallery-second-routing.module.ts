import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GallerySecondComponent } from './gallery-second-components/gallery-second.component';

const routes: Routes = [{
  path: 'gallerysecond',
  component: GallerySecondComponent,
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GallerySecondRoutingModule { }
