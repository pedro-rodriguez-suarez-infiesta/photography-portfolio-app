import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GalleryThirdComponent } from './gallery-third.component';

describe('GalleryThirdComponent', () => {
  let component: GalleryThirdComponent;
  let fixture: ComponentFixture<GalleryThirdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GalleryThirdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GalleryThirdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
