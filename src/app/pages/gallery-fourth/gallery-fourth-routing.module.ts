import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GalleryFourthComponent } from './gallery-fourth-components/gallery-fourth.component';

const routes: Routes = [
  {
    path: 'galleryfourth',
    component: GalleryFourthComponent,
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GalleryFourthRoutingModule { }
