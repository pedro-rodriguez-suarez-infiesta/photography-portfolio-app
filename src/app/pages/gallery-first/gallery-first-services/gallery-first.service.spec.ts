import { TestBed } from '@angular/core/testing';

import { GalleryFirstService } from './gallery-first.service';

describe('GalleryFirstService', () => {
  let service: GalleryFirstService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GalleryFirstService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
