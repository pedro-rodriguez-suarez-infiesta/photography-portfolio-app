export const ENDPOINTS = {
    homeMainUrl: 'http://localhost:3000/HomeMain',
    homeMainImagesUrl: 'http://localhost:3000/HomeMainImages',
    rrssUrl: 'http://localhost:3000/Rrss',
    homeAboutUrl: 'http://localhost:3000/HomeAbout',
    profileImagesUrl: 'http://localhost:3000/ProfileImages',
    galleriesMainImagesUrl: 'http://localhost:3000/GalleriesMainImages',
    galleryFirstUrl: 'http://localhost:3000/GalleryFirst',
    gallerySecondtUrl: 'http://localhost:3000/GallerySecond',
    galleryThirdUrl: 'http://localhost:3000/GalleryThird',
    galleryFourthUrl: 'http://localhost:3000/GalleryFourth',
    galleryVideoUrl: 'http://localhost:3000/GalleryVideo',
    contactMainUrl: 'http://localhost:3000/ContactMain',
    postUserUrl: 'http://localhost:3000/RegisteredUser',


};
