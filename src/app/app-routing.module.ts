import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home', pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: () => import ('./pages/home/home.module').then((m) => m.HomeModule)
  },

  {
    path: 'galleries',
    loadChildren: () => import ('./pages/galleries/galleries.module').then((m) => m.GalleriesModule),
  },
  {
    path: 'galleryfirst',
    loadChildren: () => import ('./pages/gallery-first/gallery-first.module').then((m) => m.GalleryFirstModule),
  },
  {
    path: 'gallerysecond',
    loadChildren: () => import ('./pages/gallery-second/gallery-second.module').then((m) => m.GallerySecondModule),
  },
  {
    path: 'gallerythird',
    loadChildren: () => import ('./pages/gallery-third/gallery-third.module').then((m) => m.GalleryThirdModule),
  },
  {
    path: 'galleryfourth',
    loadChildren: () => import ('./pages/gallery-fourth/gallery-fourth.module').then((m) => m.GalleryFourthModule),
  },
  {
    path: 'galleryvideo',
    loadChildren: () => import ('./pages/gallery-video/video.module').then((m) => m.VideoModule),
  },
  {
    path: 'contact',
    loadChildren: () => import ('./pages/contact/contact.module').then((m) => m.ContactModule),
  },
  {
    path: 'blog',
    loadChildren: () => import ('./pages/blog/blog.module').then((m) => m.BlogModule),
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
