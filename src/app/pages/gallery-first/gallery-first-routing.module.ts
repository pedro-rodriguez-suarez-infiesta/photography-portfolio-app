import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GalleryFirstComponent } from './gallery-first-components/gallery-first.component';

const routes: Routes = [
  {
    path: 'galleryfirst',
    component: GalleryFirstComponent,
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GalleryFirstRoutingModule { }
