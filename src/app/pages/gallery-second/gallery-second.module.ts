import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GallerySecondRoutingModule } from './gallery-second-routing.module';
import { GallerySecondComponent } from './gallery-second-components/gallery-second.component';


@NgModule({
  declarations: [GallerySecondComponent],
  imports: [
    CommonModule,
    GallerySecondRoutingModule
  ]
})
export class GallerySecondModule { }
